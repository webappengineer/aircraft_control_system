"""This module has configurations for the AC system."""

import logging
import os

CONFIG = {
    "development": "configuration.DevelopmentConfig",
    "staging": "configuration.StagingConfig",
    "production": "configuration.ProductionConfig",
    "default": "configuration.DevelopmentConfig"
}


class BaseConfig(object):
    """Base class for default set of configs."""
    DEBUG = False
    TESTING = False
    LOGGING_FORMAT = "[%(asctime)s] [%(funcName)-30s] +\
                                    [%(levelname)-6s] %(message)s"
    LOGGING_LOCATION = 'app.log'
    LOGGING_LEVEL = logging.DEBUG

    BASEDIR = os.path.abspath(os.path.dirname(__file__))


class DevelopmentConfig(BaseConfig):
    """Default set of configurations for development."""
    DEBUG = True


class StagingConfig(BaseConfig):
    """Default set of configurations for staging mode."""
    TESTING = True


class ProductionConfig(BaseConfig):
    """Default set of configurations for production."""
