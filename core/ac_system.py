import heapq

from enum import Enum, unique
from copy import deepcopy

from model.enums import SystemStatus

@unique
class SystemRequest(Enum):
    START = "START"
    STOP = "STOP"
    STATUS = "STATUS"
    ENQUEUE = "ENQUEUE"
    DEQUEUE = "DEQUEUE"
    LIST_AC = "LIST_AC"


class ControlSystem():
    queue = None
    status = SystemStatus.STOPPED

    def __init__(self):
        self.commands = {
            SystemRequest.STOP: self.stop,
            SystemRequest.START: self.start,
            SystemRequest.LIST_AC: self.list,
            SystemRequest.ENQUEUE: self.enqueue,
            SystemRequest.DEQUEUE: self.dequeue,
            SystemRequest.STATUS: self.get_status
        }

    """
    Start system
    """

    def start(self):
        success, self.status, self.queue = (True, SystemStatus.RUNNING, []) if self.status == SystemStatus.STOPPED else (False, SystemStatus.RUNNING, None)

        return success

    """
    Stop system
    """

    def stop(self):
        success, self.status, self.queue = (True, SystemStatus.STOPPED, None) if self.status == SystemStatus.RUNNING else (False, SystemStatus.STOPPED, None)

        return success

    """
    Get status of the system
    """

    def get_status(self):
        return self.status

    """
    Insert into queue
    """

    def enqueue(self, aircraft):
        if self.status == SystemStatus.RUNNING:
            heapq.heappush(self.queue, (aircraft.priority, aircraft.timestamp, aircraft))
            return True

        return False

    """
    Remove from queue
    """

    def dequeue(self):
        if self.queue is not None and len(self.queue) != 0:
            return heapq.heappop(self.queue)

        return None

    """
    Remove from queue
    """

    def list(self):
        if self.queue is not None and len(self.queue) != 0:
            q = deepcopy(self.queue)
            return [heapq.heappop(q)[2] for _ in range(len(q))]
        return []

    """
    Receives SystemRequest and executes it.
    """

    def aqm_request_process(self, request, *args):
        return self.commands[request](args) if len(args) != 0 else self.commands[request]()
