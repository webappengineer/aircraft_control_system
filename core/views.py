from flask import Blueprint, render_template, abort, request, make_response, jsonify
from flask.views import MethodView
from jinja2 import TemplateNotFound

from .ac_system import ControlSystem, SystemRequest
from model.aircraft import AirCraft
from model.enums import AirCraftType, AirCraftSize

from auth.views import require_token

_ac_system = ControlSystem()

control_system_blueprint = Blueprint('control_system', __name__, template_folder='../static/pages')


@control_system_blueprint.route('/<page>', defaults={'page': 'control_board'})
@require_token
def show(page):
    try:
        return render_template('%s.html' % page)
    except TemplateNotFound:
        abort(404)


class ManageSystemAPI(MethodView):
    """
    Acquire system status
    """

    def get(self):
        status = _ac_system.aqm_request_process(request=SystemRequest.STATUS);
        # status = _ac_system.get_status();

        return make_response(jsonify({
            'status': 'SUCCESS',
            'message': 'The system is: ' + status.name,
            'systemStatus': status.name
        })), 200

    """
    Start system
    """

    def post(self):
        response = _ac_system.aqm_request_process(request=SystemRequest.START);
        # response = _ac_system.start();
        status = _ac_system.get_status();

        if response is True:
            return make_response(jsonify({
                'status': 'SUCCESS',
                'message': 'System has been started successful',
                'systemStatus': status.name
            })), 200
        else:
            return make_response(jsonify({
                'status': 'FAILED',
                'message': 'System is already running, please stop instance first',
                'systemStatus': status.name
            })), 409

    """
    Stop system
    """

    def put(self):
        response = _ac_system.stop();
        status = _ac_system.get_status();

        if response is True:
            return make_response(jsonify({
                'status': 'SUCCESS',
                'message': 'System has been stopped successful',
                'systemStatus': status.name
            })), 200
        else:
            return make_response(jsonify({
                'status': 'FAILED',
                'message': 'No system is running',
                'systemStatus': status.name
            })), 409


class SystemAPI(MethodView):
    """
    Get the Aircrafts in the Queue
    """

    def get(self):
        response = _ac_system.list()

        if response is not None:
            # return make_response(jsonify({
            #     'status': 'SUCCESS',
            #     'message': 'Aircrafts retrieved successfully: ',
            #     'data': jsonify(eqtls=[e.serialize() for e in response])
            # })), 200
            return make_response(jsonify(data=[e.serialize() for e in response])), 200
        else:
            return make_response(jsonify({
                'status': 'FAILED',
                'message': 'Failed to get Aircrafts, possible the system is not running'
            })), 410

    """
    Enqueue Aircraft
    """

    def post(self):
        post_data = request.get_json()

        try:
            ac_type = AirCraftType[post_data.get('ac_type')]
            ac_size = AirCraftSize[post_data.get('ac_size')]

            response = _ac_system.enqueue(AirCraft(ac_type, ac_size))

            if response is True:
                return make_response(jsonify({
                    'status': 'SUCCESS',
                    'message': 'Aircraft was successfully inserted into system'
                })), 200
            else:
                return make_response(jsonify({
                    'status': 'FAILED',
                    'message': 'Failed to insert Aircraft into system, possible the system is not running'
                })), 410
        except ValueError as e:
            return make_response(jsonify({
                'status': 'FAILED',
                'message': 'Bad Request'
            })), 400

    """
    Dequeue Aircraft
    """

    def delete(self):
        response = _ac_system.dequeue()

        if response is not None:
            return make_response(jsonify({
                'status': 'SUCCESS',
                'message': 'Aircraft was successfully removed from the system' + str(response)
            })), 200
        else:
            return make_response(jsonify({
                'status': 'FAILED',
                'message': 'Failed to remove Aircraft from the system, probably queue is empty'
            })), 410


# define the API resources
system_view = SystemAPI.as_view('system_api')
manage_system_view = ManageSystemAPI.as_view('manage_system_api')

# add Rules for API Endpoints
""" GET for acquiring system status and POST for starting and PUT for stopping the system """
control_system_blueprint.add_url_rule(
    '/api/control_system/manage',
    view_func=manage_system_view,
    methods=['GET']
)
control_system_blueprint.add_url_rule(
    '/api/control_system/manage',
    view_func=manage_system_view,
    methods=['POST']
)
control_system_blueprint.add_url_rule(
    '/api/control_system/manage',
    view_func=manage_system_view,
    methods=['PUT']
)

control_system_blueprint.add_url_rule(
    '/api/control_system/queue',
    view_func=system_view,
    methods=['GET']
)

control_system_blueprint.add_url_rule(
    '/api/control_system/queue',
    view_func=system_view,
    methods=['POST']
)
control_system_blueprint.add_url_rule(
    '/api/control_system/queue',
    view_func=system_view,
    methods=['DELETE']
)
