from server import app

import unittest
import json


class ViewsTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        # creates a test client
        self.app = app.test_client()
        # propagate the exceptions to the test client
        self.app.testing = True

    def tearDown(self):
        pass

    def test_system(self):
        result = self.app.get('/api/control_system/manage')

        self.assertEqual(result.status_code, 200)
        assert b'"message": "The system is: STOPPED"' in result.data
        assert b'"status": "SUCCESS"' in result.data
        assert b'"systemStatus": "STOPPED"' in result.data

        result = self.app.post('/api/control_system/manage')

        self.assertEqual(result.status_code, 200)
        assert b'"message": "System has been started successful"' in result.data
        assert b'"status": "SUCCESS"' in result.data
        assert b'"systemStatus": "RUNNING"' in result.data

        result = self.app.post('/api/control_system/queue', data=json.dumps(dict(ac_type='PASSENGER', ac_size='LARGE')), content_type='application/json')

        self.assertEqual(result.status_code, 200)
        assert b'"message": "Aircraft was successfully inserted into system"' in result.data
        assert b'"status": "SUCCESS"' in result.data

        result = self.app.post('/api/control_system/queue', data=json.dumps(dict(ac_type='CARGO', ac_size='LARGE')), content_type='application/json')

        self.assertEqual(result.status_code, 200)
        assert b'"message": "Aircraft was successfully inserted into system"' in result.data
        assert b'"status": "SUCCESS"' in result.data

        result = self.app.get('/api/control_system/queue')

        self.assertEqual(result.status_code, 200)
        assert b'"priority": 1' in result.data
        assert b'"priority": 3' in result.data

        result = self.app.delete('/api/control_system/queue')

        self.assertEqual(result.status_code, 200)
        assert b'"message": "Aircraft was successfully removed from the system(1' in result.data
        assert b'"message": "Aircraft was successfully removed from the system(3' not in result.data

        result = self.app.get('/api/control_system/queue')

        self.assertEqual(result.status_code, 200)
        assert b'"priority": 1' not in result.data
        assert b'"priority": 3' in result.data

        result = self.app.put('/api/control_system/manage')

        self.assertEqual(result.status_code, 200)
        assert b'"message": "System has been stopped successful"' in result.data
