from unittest import TestCase, main

from core.ac_system import ControlSystem, SystemRequest
from model.aircraft import AirCraft
from model.enums import SystemStatus, AirCraftType, AirCraftSize


class AcSystemTestCase(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    @staticmethod
    def test_command_setup():
        control_system = ControlSystem()

        assert len(control_system.commands) != 0
        assert SystemRequest.START in control_system.commands
        assert SystemRequest.STATUS in control_system.commands
        assert SystemRequest.STOP in control_system.commands
        assert SystemRequest.LIST_AC in control_system.commands
        assert SystemRequest.ENQUEUE in control_system.commands
        assert SystemRequest.DEQUEUE in control_system.commands

    @staticmethod
    def test_start():
        control_system = ControlSystem()

        assert control_system.status is SystemStatus.STOPPED
        assert control_system.start() is True
        assert control_system.status is SystemStatus.RUNNING
        assert len(control_system.queue) == 0

    @staticmethod
    def test_stop():
        control_system = ControlSystem()

        assert control_system.status is SystemStatus.STOPPED
        assert control_system.start() is True
        assert control_system.status is SystemStatus.RUNNING
        assert len(control_system.queue) == 0

        assert control_system.stop() is True
        assert control_system.status is SystemStatus.STOPPED
        assert control_system.queue is None

    @staticmethod
    def test_status():
        control_system = ControlSystem()

        assert control_system.status is SystemStatus.STOPPED
        assert control_system.aqm_request_process(SystemRequest.START) is True
        assert control_system.status is SystemStatus.RUNNING
        assert control_system.aqm_request_process(SystemRequest.STOP) is True
        assert control_system.status is SystemStatus.STOPPED

    @staticmethod
    def test_enqueue():
        control_system = ControlSystem()

        assert control_system.start() is True
        assert control_system.enqueue(AirCraft(AirCraftType.PASSENGER, AirCraftSize.LARGE)) is True
        assert len(control_system.queue) is 1
        assert control_system.enqueue(AirCraft(AirCraftType.CARGO, AirCraftSize.LARGE))
        assert len(control_system.queue) is 2
        assert control_system.stop() is True
        assert control_system.queue is None

        assert control_system.enqueue(None) is False
        assert control_system.enqueue(AirCraft(AirCraftType.PASSENGER, AirCraftSize.SMALL)) is False

    @staticmethod
    def test_dequeue():
        control_system = ControlSystem()

        assert control_system.start() is True
        assert control_system.enqueue(AirCraft(AirCraftType.PASSENGER, AirCraftSize.LARGE))
        assert len(control_system.queue) is 1
        assert control_system.enqueue(AirCraft(AirCraftType.CARGO, AirCraftSize.LARGE))
        assert len(control_system.queue) is 2
        assert control_system.dequeue() is not None
        assert len(control_system.queue) is 1
        assert control_system.dequeue() is not None
        assert len(control_system.queue) is 0
        assert control_system.stop() is True
        assert control_system.queue is None

        assert control_system.dequeue() is None

    @staticmethod
    def test_priority():
        control_system = ControlSystem()

        assert control_system.start() is True
        ac1 = AirCraft(AirCraftType.PASSENGER, AirCraftSize.SMALL)
        ac2 = AirCraft(AirCraftType.PASSENGER, AirCraftSize.LARGE)

        assert control_system.enqueue(ac1) is True
        assert control_system.enqueue(ac2) is True

        q = control_system.list()
        assert q[0] == ac2 and q[1] == ac1

        ac3 = AirCraft(AirCraftType.PASSENGER, AirCraftSize.LARGE)
        assert control_system.enqueue(ac3) is True

        q = control_system.list()
        assert q[0] == ac2 and q[1] == ac3 and q[2] == ac1

        ac4 = AirCraft(AirCraftType.CARGO, AirCraftSize.SMALL)
        assert control_system.enqueue(ac4) is True

        q = control_system.list()
        assert q[0] == ac2 and q[1] == ac3 and q[2] == ac1 and q[3] == ac4

        ac5 = AirCraft(AirCraftType.CARGO, AirCraftSize.LARGE)
        assert control_system.enqueue(ac5) is True

        q = control_system.list()
        assert q[0] == ac2 and q[1] == ac3 and q[2] == ac1 and q[3] == ac5 and q[4] == ac4

        ac6 = AirCraft(AirCraftType.PASSENGER, AirCraftSize.LARGE)
        assert control_system.enqueue(ac6) is True

        q = control_system.list()
        assert q[0] == ac2 and q[1] == ac3 and q[2] == ac6 and q[3] == ac1 and q[4] == ac5 and q[5] == ac4


if __name__ == '__main__':
    main()
