$(function(){
   $('#username').val('secret');
   $('#password').val('admin');

   var aircrafts = [];
   var systemStatus = 'UNKNOWN';

   function setSystemStatus(status){
        systemStatus = status;

        $("#systemStatus").text(systemStatus);

        if(systemStatus === "STOPPED"){
            $("#txtStartStop").text("START");
        } else{
            $("#txtStartStop").text("STOP");
        }
   }

   function fetchAircraftList(){
        $.ajax({
            url: 'api/control_system/queue',
            type: 'GET',
            success: function(response){
                console.log(response.data);

                if(response.data){
                    $("#aircraftList tbody").empty();
                    $.each(response.data, function (i, item) {
                        $("#aircraftList tbody").append(
                            "<tr>"
                              +"<td>"+response.data[i].priority+"</td>"
                              +"<td>"+response.data[i].ac_type+"</td>"
                              +"<td>"+response.data[i].ac_size+"</td>"
                              +"<td>"+response.data[i].timestamp+"</td>"
                            +"</tr>" );
                    });
                }

            },
            error: function(error){
                console.log(error);
            }
        });
   }

   fetchAircraftList();

   $.ajax({
        url: 'api/control_system/manage',
        type: 'GET',
        success: function(response){
            console.log(response);
            if (response.status === 'SUCCESS') {
                setSystemStatus(response.systemStatus);
            }
        },
        error: function(error){
            console.log(error);
            setSystemStatus('UNKNOWN');
        }
	});



	$('#btnLogin').click(function(){
	    var username = $('#username').val();
        var password = $('#password').val();

        //TODO encrypt password before sending to the backend.

		$.ajax({
			url: '/api/auth',
			data: JSON.stringify({ username: username, password: password }),
			type: 'POST',
			contentType: "application/json",
			success: function(response){
				console.log(response);
				if (response.status === 'success' && response.redirect) {
                    // response.redirect contains the string URL to redirect to
                    window.location.href = response.redirect;
                }
			},
			error: function(error){
				console.log(error);
			}
		});
	});

    $('#btnStartStop').click(function(){
        var type = "POST";

        if(systemStatus == 'RUNNING'){
            type = "PUT";
        }

		$.ajax({
			url: '/api/control_system/manage',
			data: JSON.stringify({}),
			type: type,
			contentType: "application/json",
			success: function(response){
				console.log(response);
				if (response.status === 'SUCCESS') {
				    setSystemStatus(response.systemStatus);
                }
			},
			error: function(error){
				console.log(error);
				setSystemStatus('UNKNOWN');
			}
		});
	});

	$('#btnQueue').click(function(){
	    var ac_type = $('#acType').val();
        var ac_size = $('#acSize').val();

		$.ajax({
			url: '/api/control_system/queue',
			data: JSON.stringify({ ac_type: ac_type, ac_size: ac_size }),
			type: 'POST',
			contentType: "application/json",
			success: function(response){
				console.log(response);
				if (response.status === 'SUCCESS') {
				    fetchAircraftList();
                }
			},
			error: function(error){
				console.log(error);
			}
		});
	});

	$('#btnDequeue').click(function(){
		$.ajax({
			url: '/api/control_system/queue',
			data: JSON.stringify({}),
			type: 'DELETE',
			contentType: "application/json",
			success: function(response){
				console.log(response);
				if (response.status === 'SUCCESS') {
				    fetchAircraftList();
                }
			},
			error: function(error){
				console.log(error);
			}
		});
	});
});