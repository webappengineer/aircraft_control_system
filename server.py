"""The server Aircraft Control System."""

import logging.handlers
import os
import traceback

from flask import Flask
from gevent.wsgi import WSGIServer

from auth.views import auth_blueprint
from configuration import CONFIG
from core.views import control_system_blueprint


def create_app():
    """Configure application"""
    app = Flask(__name__)
    config_name = os.getenv('AIRCRAFT_CONFIG_NAME', 'default')

    app.config.from_object(CONFIG[config_name])

    return app

app = create_app()

app.register_blueprint(auth_blueprint)
app.register_blueprint(control_system_blueprint)

logging.basicConfig()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh = logging.handlers.RotatingFileHandler('./app.log', maxBytes=10240, backupCount=5)
fh.setFormatter(formatter)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(fh)


@app.before_first_request
def init():
    """Initialize the application with defaults."""


def main():
    """Main entry point of the system."""

    try:
        port = int(os.environ.get('PORT', 9090))
        http_server = WSGIServer(('0.0.0.0', port),
                                 app,
                                 log=logger,
                                 error_log=logger)

        logger.info('starting server')
        http_server.serve_forever()
    except Exception as exc:
        logger.error(exc.message)
        logger.exception(traceback.format_exc())
    finally:
        pass