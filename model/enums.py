from enum import Enum, unique, auto


class OrderedEnum(Enum):
    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self.value >= other.value
        return NotImplemented

    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self.value > other.value
        return NotImplemented

    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self.value <= other.value
        return NotImplemented

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented


@unique
class AirCraftType(OrderedEnum):
    PASSENGER = auto()
    CARGO = auto()


@unique
class AirCraftSize(OrderedEnum):
    LARGE = auto()
    SMALL = auto()


@unique
class AircraftPriority(OrderedEnum):
    PASSENGER_LARGE = auto()
    PASSENGER_SMALL = auto()
    CARGO_LARGE = auto()
    CARGO_SMALL = auto()


@unique
class SystemStatus(Enum):
    STOPPED = auto()
    RUNNING = auto()
    STARTING = auto()
