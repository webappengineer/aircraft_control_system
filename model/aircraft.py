import time

from functools import total_ordering
from .enums import AircraftPriority


@total_ordering
class AirCraft(object):
    def __init__(self, ac_type, ac_size):
        self.ac_type = ac_type
        self.ac_size = ac_size
        self.timestamp = int(time.time())

        self.priority = AircraftPriority[ac_type.name + '_' + ac_size.name].value

        return

    def __eq__(self, other):
        try:
            return self.priority == other.priority and self.ac_size == other.ac_size and self.ac_type == other.ac_type and self.timestamp and other.timestamp
        except AttributeError:
            return NotImplemented

    def __lt__(self, other):
        try:
            return self.priority <= other.priority
        except AttributeError:
            return NotImplemented

    def __str__(self):
        return 'type:' + str(self.ac_type) + ' size:' + str(self.ac_size) + ' time: ' + str(self.timestamp) + ' priority:' + str(self.priority)

    def serialize(self):
        return {
            'ac_type': self.ac_type.name,
            'ac_size': self.ac_size.name,
            'timestamp': self.timestamp,
            'priority': self.priority
        }
