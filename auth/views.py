import jwt
import datetime

from functools import wraps

from flask import Blueprint, render_template, abort, request, make_response, jsonify
from flask.views import MethodView
from jinja2 import TemplateNotFound

auth_blueprint = Blueprint('auth', __name__, template_folder='../static/pages')


@auth_blueprint.route('/', defaults={'page': 'login'})
def show(page):
    try:
        return render_template('%s.html' % page)
    except TemplateNotFound:
        abort(404)


def encode_auth_token(username):
    """
    Generates the Auth Token
    :return: string
    """
    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, seconds=3600),
            'iat': datetime.datetime.utcnow(),
            'sub': username
        }
        return jwt.encode(
            payload,
            'SECRET_KEY',
            algorithm='HS256'
        )
    except Exception as e:
        return e


def decode_auth_token(auth_token):
    """
    Validates the auth token
    :param auth_token:
    :return: integer|string
    """
    try:
        payload = jwt.decode(auth_token, 'SECRET_KEY')

        return payload['sub']
    except jwt.ExpiredSignatureError:
        return 'Signature has expired'
    except jwt.InvalidTokenError:
        return 'Token is invalid.'


# Decorator function
def require_token(view_function):
    @wraps(view_function)
    # the new, post-decoration function. Note *args and **kwargs here.
    def decorated_function(*args, **kwargs):
        auth_token = request.args.get('token')

        if auth_token and isinstance(decode_auth_token(auth_token), str):
            return view_function(*args, **kwargs)
        else:
            abort(401)

    return decorated_function


class AuthenticationAPI(MethodView):
    """
    Login in user
    """

    def post(self):
        post_data = request.get_json()

        try:
            if post_data.get('username') == 'secret' and post_data.get('password') == 'admin':
                auth_token = encode_auth_token(post_data.get('username'))

                return make_response(jsonify({
                    'status': 'success',
                    'message': 'Login was successfully.',
                    'redirect': '/control_board?token=' + auth_token.decode(),
                    'token': auth_token.decode()
                })), 200
            else:
                return make_response(jsonify({
                    'status': 'fail',
                    'message': 'Failed authentication.'
                })), 401
        except Exception as e:
            print(e)
            return make_response(jsonify({
                'status': 'fail',
                'message': 'Try again'
            })), 500

    """
    Logout
    """

    def put(self):
        put_data = request.get_json()
        try:
            if put_data.get('username') == "secret":
                return make_response(jsonify({
                    'status': 'success',
                    'message': 'Logout was successfully.'
                })), 200
            else:
                return make_response(jsonify({
                    'status': 'fail',
                    'message': 'Cannot logout unknown user.'
                })), 403
        except Exception as e:
            return make_response(jsonify({
                'status': 'fail',
                'message': 'Try again'
            })), 500


# define the API resources
authentication_view = AuthenticationAPI.as_view('authentication_api')

# add Rules for API Endpoints
"""POST for login and PUT for logout"""
auth_blueprint.add_url_rule(
    '/api/auth',
    view_func=authentication_view,
    methods=['POST']
)
auth_blueprint.add_url_rule(
    '/api/auth',
    view_func=authentication_view,
    methods=['PUT']
)
