# Air Traffic Control System
## Specifications / Requirements
- ###Summary
A software subsystem of an air-traffic control system is defined to manage a queue of aircraft (AC) in an airport. The aircraft queue is managed by a process that responds to three types of requests:
    - System boot used to start the system.
    - Enqueue aircraft used to insert a new AC into the system.
    - Dequeue aircraft used to remove an AC from the system.

- ###AC’s have at least (but are not limited to having) the following properties:
    - AC type: Passenger or Cargo
    - AC size: Small or Large

- ###Operation:
    - There is no limit to the number of AC’s it can manage.
    - Dequeue aircraft requests result in selection of one AC for removal such that:
        - Passenger AC’s have removal precedence over Cargo AC’s.
        - Large AC’s of a given type have removal precedence over Small AC’s of the same type.
        - Earlier enqueued AC’s of a given type and size have precedence over later enqueued AC’s of the same type and size.

## Project Prerequisites
  - Python3.6 : https://www.python.org/downloads/release/python-362/

## Project Setup

```sh
$ cd path/to/your/projects
$ mkdir aircraft_control_system
$ cd aircraft_control_system
$ git clone https://mromondi@bitbucket.org/webappengineer/aircraft_control_system.git
$ pip install -r requirements.txt
```

## Run application

- Run
```
$ python run.py
```

## Preview application
```
    - On heroku - https://aircraft-control-system.herokuapp.com
```
```
    - Locally - Open http://localhost:9090/ on your browser
```

## Testing

 - Run Tests
  ```
  $ python -m unittest test\ac_system_test.py
  $ python -m unittest test\views_test.py
  ```